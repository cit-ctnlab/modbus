dbLoadDatabase("$(EPICS_ROOT)/modules/modbus/dbd/modbus.dbd")
modbus_registerRecordDeviceDriver(pdbbase)

#### Vacuum Can Temperature Sensor ADC ####
drvAsynIPPortConfigure("c3VCTMPSNS","10.0.1.51:502",0,0,1)
modbusInterposeConfig("c3VCTMPSNS",0,5000,0)
drvModbusAsynConfigure("ADC_Reg_VCTMPSNS","c3VCTMPSNS",0,4,0,8,4,32,"Acromag")

dbLoadDatabase("/home/modbus/VacCanTempSensors.db")

iocInit
