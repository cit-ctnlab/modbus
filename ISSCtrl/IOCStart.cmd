dbLoadDatabase("$(EPICS_ROOT)/modules/modbus/dbd/modbus.dbd")
modbus_registerRecordDeviceDriver(pdbbase)

drvAsynIPPortConfigure("c3ISS","10.0.1.40:502",0,0,1)
modbusInterposeConfig("c3ISS",0,5000,0)
drvModbusAsynConfigure("BIO_Reg_ISS","c3ISS",0,6,1,4,0,0,"Acromag")

dbLoadDatabase("/home/modbus/ISSControls.db")

iocInit
