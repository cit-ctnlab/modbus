# This IOC process load all slow monitor and control channels 

dbLoadDatabase("$(EPICS_ROOT)/modules/modbus/dbd/modbus.dbd")
modbus_registerRecordDeviceDriver(pdbbase)

drvAsynIPPortConfigure("c3test1","10.0.1.41:502",0,0,1)
modbusInterposeConfig("c3test1",0,5000,0)
drvModbusAsynConfigure("DAC_Reg_1","c3test1",0,6,1,8,4,0,"Acromag")
drvModbusAsynConfigure("BIO_Reg_1","c3test1",0,5,0,4,0,0,"Acromag") 


drvAsynIPPortConfigure("maul","10.0.1.42:502",0,0,1)
modbusInterposeConfig("maul",0,5000,0)
drvModbusAsynConfigure("ADC_Reg_1","maul",0,4,0,8,4,32,"Acromag")


drvAsynIPPortConfigure("vader","10.0.1.50:502",0,0,1)
modbusInterposeConfig("vader",0,5000,0)
drvModbusAsynConfigure("ADC_Reg_TempSense","vader",0,4,0,8,4,32,"Acromag")


dbLoadDatabase("/home/modbus/LaserSlowControlsAndMonitors.db")


dbLoadDatabase("/home/modbus/TempCtrlCav.db")
dbLoadDatabase("/home/modbus/TempCtrlCan.db")

dbLoadDatabase("/home/modbus/PLLMon.db")

iocInit
