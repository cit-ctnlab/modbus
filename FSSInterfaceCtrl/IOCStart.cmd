dbLoadDatabase("$(EPICS_ROOT)/modules/modbus/dbd/modbus.dbd")
modbus_registerRecordDeviceDriver(pdbbase)

drvAsynIPPortConfigure("c3FSSS","10.0.1.44:502",0,0,1)
modbusInterposeConfig("c3FSSS",0,5000,0)
drvModbusAsynConfigure("DAC_Reg_FSSS","c3FSSS",0,6,1,8,4,0,"Acromag")
drvModbusAsynConfigure("BIO_Reg_FSSS","c3FSSS",0,5,0,4,0,0,"Acromag") 

drvAsynIPPortConfigure("c3FSSN","10.0.1.45:502",0,0,1)
modbusInterposeConfig("c3FSSN",0,5000,0)
drvModbusAsynConfigure("DAC_Reg_FSSN","c3FSSN",0,6,1,8,4,0,"Acromag")
drvModbusAsynConfigure("BIO_Reg_FSSN","c3FSSN",0,5,0,4,0,0,"Acromag") 

drvAsynIPPortConfigure("c3FSSADC","10.0.1.43:502",0,0,1)
modbusInterposeConfig("c3FSSADC",0,5000,0)
drvModbusAsynConfigure("ADC_Reg_FSS","c3FSSADC",0,4,0,8,4,32,"Acromag")


dbLoadDatabase("/home/modbus/FSSInterfaceControls.db")

iocInit
