dbLoadDatabase("$(EPICS_ROOT)/modules/modbus/dbd/modbus.dbd")
modbus_registerRecordDeviceDriver(pdbbase)

## PMC North controls ##
drvAsynIPPortConfigure("c3PMCNDAC","10.0.1.47:502",0,0,1)
modbusInterposeConfig("c3PMCNDAC",0,5000,0)
drvModbusAsynConfigure("DAC_Reg_PMCN","c3PMCNDAC",0,6,1,8,4,0,"Acromag")
drvModbusAsynConfigure("BIO_Reg_PMCN","c3PMCNDAC",0,5,0,4,0,0,"Acromag")

drvAsynIPPortConfigure("c3PMCNADC","10.0.1.46:502",0,0,1)
modbusInterposeConfig("c3PMCNADC",0,5000,0)
drvModbusAsynConfigure("ADC_Reg_PMCN","c3PMCNADC",0,4,0,8,4,32,"Acromag")

## PMC South controls ##
drvAsynIPPortConfigure("c3PMCSDAC","10.0.1.49:502",0,0,1)
modbusInterposeConfig("c3PMCSDAC",0,5000,0)
drvModbusAsynConfigure("DAC_Reg_PMCS","c3PMCSDAC",0,6,1,8,4,0,"Acromag")
drvModbusAsynConfigure("BIO_Reg_PMCS","c3PMCSDAC",0,5,0,4,0,0,"Acromag")

drvAsynIPPortConfigure("c3PMCSADC","10.0.1.48:502",0,0,1)
modbusInterposeConfig("c3PMCSADC",0,5000,0)
drvModbusAsynConfigure("ADC_Reg_PMCS","c3PMCSADC",0,4,0,8,4,32,"Acromag")



dbLoadDatabase("/home/modbus/NPMCInterfaceControls.db")
dbLoadDatabase("/home/modbus/SPMCInterfaceControls.db")


iocInit
